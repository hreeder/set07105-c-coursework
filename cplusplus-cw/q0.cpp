#include "q0.h"

using namespace std;

void q0() {
	/* 	Table Printing
		Each row should contain
		Deg C 	approx F	Actual F	Diff
	*/ 

	cout << "C\tApprox F\tActual F\tDifference" << endl;
	for (int i=0;i<50;i++) {cout << "=";}
	cout << endl;


	for (int c=0;c<=100;c++) {
		// Print Celsius Value
		cout << c << "\t";

		// Print Approximate F
		// Formula: Double, +30
		int approxF = (c * 2) + 30;
		cout << approxF << "\t\t";

		// Print Actual F
		// Formula: ((C * 9) / 5) + 32
		int realF = ((c * 9) / 5) + 32;
		cout << realF << "\t\t";

		// Print Difference
		cout << approxF - realF << endl;
	}
	for (int i=0;i<50;i++) {cout << "=";}
	cout << endl << endl;
}