//
//  q4.cpp
//  cplusplus-cw
//
//  Created by Info Serv on 25/02/2013.
//  Copyright (c) 2013 Harry Reeder. All rights reserved.
//

#include "q4.h"

using namespace std;

void q4() {
    string line;
    ifstream inFile ("marks2.txt");
    vector<int> marks;      //Create a vector to hold the marks
    
    if (inFile.is_open()) {
        while (inFile.good()) {
            int buff;
            while (inFile >> buff) {
                marks.push_back(buff);
            }
        }
        inFile.close();
    }
    else {
        cout << "Unable to open file" << endl;
    }
    
    int marksDistrib [10] = {0,0,0,0,0,0,0,0,0,0};
    
    for (int i=0;i < marks.size(); i++) {
        if (marks[i] < 11) {
            marksDistrib[0]++;
        } else if (marks[i] < 21) {
            marksDistrib[1]++;
        } else if (marks[i] < 31) {
            marksDistrib[2]++;
        } else if (marks[i] < 41) {
            marksDistrib[3]++;
        } else if (marks[i] < 51) {
            marksDistrib[4]++;
        } else if (marks[i] < 61) {
            marksDistrib[5]++;
        } else if (marks[i] < 71) {
            marksDistrib[6]++;
        } else if (marks[i] < 81) {
            marksDistrib[7]++;
        } else if (marks[i] < 91) {
            marksDistrib[8]++;
        } else if (marks[i] < 101) {
            marksDistrib[9]++;
        }
    }
    
    cout << "Marks Distribution:" << endl;
    
    cout << " 0 -  10 | ";
    for (int i=0;i < marksDistrib[0]; i++) {
        cout << "*";
    }
    cout << endl;
    
    cout << "11 -  20 | ";
    for (int i=0;i < marksDistrib[1]; i++) {
        cout << "*";
    }
    cout << endl;
    
    cout << "21 -  30 | ";
    for (int i=0;i < marksDistrib[2]; i++) {
        cout << "*";
    }
    cout << endl;
    
    cout << "31 -  40 | ";
    for (int i=0;i < marksDistrib[3]; i++) {
        cout << "*";
    }
    cout << endl;
    
    cout << "41 -  50 | ";
    for (int i=0;i < marksDistrib[4]; i++) {
        cout << "*";
    }
    cout << endl;
    
    cout << "51 -  60 | ";
    for (int i=0;i < marksDistrib[5]; i++) {
        cout << "*";
    }
    cout << endl;
    
    cout << "61 -  70 | ";
    for (int i=0;i < marksDistrib[6]; i++) {
        cout << "*";
    }
    cout << endl;
    
    cout << "71 -  80 | ";
    for (int i=0;i < marksDistrib[7]; i++) {
        cout << "*";
    }
    cout << endl;
    
    cout << "81 -  90 | ";
    for (int i=0;i < marksDistrib[8]; i++) {
        cout << "*";
    }
    cout << endl;
    
    cout << "91 - 100 | ";
    for (int i=0;i < marksDistrib[9]; i++) {
        cout << "*";
    }
    cout << endl << endl;
}