//
//  q5.cpp
//  cplusplus-cw
//
//  Created by Info Serv on 25/02/2013.
//  Copyright (c) 2013 Harry Reeder. All rights reserved.
//

#include "q5.h"

using namespace std;

void q5() {
    string line;
    ifstream inFile ("palindrome-yes.txt");
    vector<int> numbers;
    
    if (inFile.is_open()) {
        while (inFile.good()) {
            int buff;
            while (inFile >> buff) {
                numbers.push_back(buff);
            }
        }
    }
    
    int midpoint = numbers.size() / 2;
    
    cout << "Normal:  ";
    
    bool palindromeFlag = true;
    
    for (int i = 0; i < numbers.size(); i++) {
        cout << numbers[i] << " ";
    }
    cout << endl;

    cout << "Reverse: ";
    
    for (int i = numbers.size()-1; i > -1; i--) {
        cout << numbers[i] << " ";
    }
    cout << endl;
    
    for (int i = 0; i < midpoint; i++) {
        int mirror = numbers.size() - i - 1;
        
        if (numbers[i] != numbers[mirror]) {
            palindromeFlag = false;
        }
    }
    
    cout << "Palindrome: ";
    if (palindromeFlag) {
        cout << "Yes" << endl << endl;
    } else {
        cout << "No" << endl << endl;
    }
    
}