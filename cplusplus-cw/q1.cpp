#include "q1.h"

using namespace std;

void q1() {
	/*
		Write a program that prints a table
		of values (x, sqrt(x), x^2, x^3) for
		all numbers starting at 1 and up to
		and including a number which will be
		input from the console. Also inc totals
	*/

	int max;
	int xTotal = 0;
	int sqrtTotal = 0;
	int squareTotal = 0;
	int cubeTotal = 0;

	cout << "Input Maximum Number: ";
	cin >> max;
	cout << endl;

	cout << "\tx\tsqrt(x)\t\tx^2\tx^3" << endl;
	for (int i=0;i<45;i++) {cout << "=";}
	cout << endl;

	for (int x = 0;x<=max;x++) {
		cout << "\t" << x << "\t";
		xTotal += x;
		
		cout << sqrt (x) << "\t\t";
		sqrtTotal += sqrt (x);

		cout << x*x << "\t";
		squareTotal += x*x;

		cout << x*x*x << "\t";
		cubeTotal += x*x*x;

		cout << endl;
	}

	for (int i=0;i<45;i++) {cout << "=";}
	cout << endl;

	cout << "Total\t";
	cout << xTotal << "\t";
	cout << sqrtTotal << "\t\t";
	cout << squareTotal << "\t";
	cout << cubeTotal << "\t";
	cout << endl << endl;
}