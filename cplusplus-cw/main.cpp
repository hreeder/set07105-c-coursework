//
//  main.cpp
//  cplusplus-cw
//
//  Created by Info Serv on 14/02/2013.
//  Copyright (c) 2013 Harry Reeder. All rights reserved.
//

#include "stdafx.h"

using namespace std;

int main(int argc, const char * argv[]) {
    cout << "Harry Reeder, 40052308\n";
    cout << "Introduction to C++ Coursework\n\n";
    
    cout << "Question 0" << endl;
    q0();

    cout << "Question 1" << endl;
    q1();

    cout << "Question 4" << endl;
    q4();
    
    cout << "Question 5" << endl;
    q5();

    // cout << "Question 6" << endl;
    // q6();
    
    return 0;
}